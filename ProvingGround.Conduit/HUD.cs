using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ProvingGround.Conduit.DrawObjects;
using ProvingGround.Conduit.Utilities;
using Rhino;
using Rhino.Display;
using Rhino.Geometry;
using Svg;
using Svg.Pathing;

namespace ProvingGround.Conduit
{
    public class HUD : DisplayConduit
    {
        private readonly Interval2d _bounds;
        private readonly List<DrawObject> _drawObjects;
        private readonly HashSet<string> _viewportFilters;

        private Plane _drawPlane;
        private UnitsPerPixel _unitsPerPixel;

        /// <summary>
        /// Initializes a new instance of the <see cref="HUD"/> class.
        /// </summary>
        /// <param name="bounds">The rectangular bounds for setting the relative location of drawing objects.</param>
        /// <param name="drawObjects">The list of drawing objects to apply in the HUD.</param>
        /// <param name="viewportFilters">The viewport filters to be employed (if any).</param>
        public HUD(Rectangle3d bounds, List<DrawObject> drawObjects, string[] viewportFilters)
        {
            _bounds = new Interval2d(bounds);
            _drawObjects = drawObjects;
            if (viewportFilters.Any())
            {
                _viewportFilters = new HashSet<string>(viewportFilters);
            }
        }

        /// <summary>
        /// Prepares the draw objects based on the specified bounds.
        /// </summary>
        public void PrepareDrawObjects()
        {
            foreach (DrawObject drawObject in _drawObjects)
            {
                drawObject.SetupWithViewBounds(_bounds);
            }
        }

        public void ViewAndSVGCapture(string filePath, RhinoDoc doc, double scale)
        {
            if (doc == null)
            {
                throw new ArgumentNullException(nameof(doc));
            }

            RhinoView rhinoView = doc.Views.ActiveView;

            if (_viewportFilters == null || _viewportFilters.Contains(rhinoView.MainViewport.Name))
            {
                double width = rhinoView.MainViewport.Size.Width * scale;
                double height = rhinoView.MainViewport.Size.Height * scale;

                var viewBounds = new Interval2d(
                    new Interval(0, width),
                    new Interval(0, height));

                var fSvgDoc = new SvgDocument
                {
                    Width = (float)width,
                    Height = (float)height,
                };

                fSvgDoc.ViewBox = new SvgViewBox(0, 0, (float)width, (float)height);

                var group = new SvgGroup();
                fSvgDoc.Children.Add(group);

                foreach (var drawObject in _drawObjects)
                {
                    drawObject.AddSVG(viewBounds, group);
                }

                fSvgDoc.Write(filePath);
            }

            //group.Children.Add(new SvgCircle
            //{
            //    CenterX = 150,
            //    CenterY = 150,
            //    Radius = 100,
            //    Fill = new SvgColourServer(Color.Red),
            //    Stroke = new SvgColourServer(Color.Black),
            //    StrokeWidth = 1,
            //});

            //group.Children.Add(new SvgLine
            //{
            //    StartX = 250,
            //    StartY = 250,
            //    EndX = 400,
            //    EndY = 400,
            //    Stroke = new SvgColourServer(Color.Blue),
            //    StrokeWidth = 5,
            //});

            //var polyline = new SvgPolyline
            //{
            //    Points = new SvgPointCollection(),
            //};
            //polyline.Points.Add(new SvgUnit(400));
            //polyline.Points.Add(new SvgUnit(400));
            //polyline.Points.Add(new SvgUnit(500));
            //polyline.Points.Add(new SvgUnit(400));
            //polyline.Points.Add(new SvgUnit(500));
            //polyline.Points.Add(new SvgUnit(500));
            //polyline.Points.Add(new SvgUnit(400));
            //polyline.Points.Add(new SvgUnit(500));
            //polyline.Points.Add(new SvgUnit(400));
            //polyline.Points.Add(new SvgUnit(400));
            //polyline.Stroke = new SvgColourServer(Color.Red);
            //polyline.Fill = new SvgColourServer(Color.Green);

            //group.Children.Add(polyline);

            //var path = new SvgPath
            //{
            //    PathData = new SvgPathSegmentList(),
            //};
            //var lineSegment1 = new SvgLineSegment(new PointF(250, 250), new PointF(400, 400));
            //var lineSegment2 = new SvgLineSegment(new PointF(400, 400), new PointF(400, 250));
            //path.PathData.Add(lineSegment1);
            //path.PathData.Add(lineSegment2);

            //path.Stroke = new SvgColourServer(Color.Green);
            //group.Children.Add(path);
        }

        /// <summary>
        /// Library developers should override this function to increase the bounding box of scene so it includes the
        /// geometry that you plan to draw in the "Draw" virtual functions.
        /// <para>The default implementation does nothing.</para>
        /// </summary>
        /// <param name="e">The event argument contain the current bounding box state.</param>
        /// <example>
        ///   <code source="examples\vbnet\ex_meshdrawing.vb" lang="vbnet" />
        ///   <code source="examples\cs\ex_meshdrawing.cs" lang="cs" />
        ///   <code source="examples\py\ex_meshdrawing.py" lang="py" />
        /// </example>
        protected override void CalculateBoundingBox(CalculateBoundingBoxEventArgs e)
        {
            if (e == null)
            {
                throw new ArgumentNullException(nameof(e));
            }

            if (_viewportFilters != null && !_viewportFilters.Contains(e.Viewport.Name))
            {
                return;
            }

            base.CalculateBoundingBox(e);

            Point3d[] farCorners = e.Viewport.GetFarRect();
            Point3d[] nearCorners = e.Viewport.GetNearRect();

            var viewCorners = new Point3d[4]
              {
                (farCorners[0] * 0.15) + (nearCorners[0] * 0.85),
                (farCorners[1] * 0.15) + (nearCorners[1] * 0.85),
                (farCorners[2] * 0.15) + (nearCorners[2] * 0.85),
                (farCorners[3] * 0.15) + (nearCorners[3] * 0.85),
              };

            var b = new BoundingBox(viewCorners);

            _drawPlane = new Plane(viewCorners[0], viewCorners[1], viewCorners[2]);

            _unitsPerPixel = new UnitsPerPixel(
              viewCorners[0].DistanceTo(viewCorners[1]) / e.Viewport.Size.Width,
              viewCorners[0].DistanceTo(viewCorners[2]) / e.Viewport.Size.Height);

            e.IncludeBoundingBox(b);
        }

        /// <summary>
        /// Called after all non-highlighted objects have been drawn and PostDrawObjects has been called.
        /// Depth writing and testing are turned OFF. If you want to draw with depth writing/testing,
        /// see PostDrawObjects.
        /// <para>The default implementation does nothing.</para>
        /// </summary>
        /// <param name="e">The event argument contains the current viewport and display state.</param>
        protected override void DrawForeground(Rhino.Display.DrawEventArgs e)
        {
            if (e == null)
            {
                throw new ArgumentNullException(nameof(e));
            }

            if (_viewportFilters != null && !_viewportFilters.Contains(e.Viewport.Name))
            {
                return;
            }

            foreach (DrawObject drawObject in _drawObjects)
            {
                drawObject.Draw(e, _drawPlane, _unitsPerPixel);
            }
        }
    }
}
