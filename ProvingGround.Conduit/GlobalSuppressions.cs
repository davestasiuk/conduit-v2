﻿
// This file is used by Code Analysis to maintain SuppressMessage 
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given 
// a specific target and scoped to a namespace, type, member, etc.

[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Performance", "CA1822:Mark members as static", Justification = "<Pending>", Scope = "member", Target = "~M:ProvingGround.Conduit.HUD.ViewAndSVGCapture(System.String,System.String,Rhino.RhinoDoc,System.Double)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = "<Pending>", Scope = "member", Target = "~M:ProvingGround.Conduit.DrawObjects.DrawLine.AddSVG(ProvingGround.Conduit.Utilities.Interval2d,Svg.SvgGroup)")]

