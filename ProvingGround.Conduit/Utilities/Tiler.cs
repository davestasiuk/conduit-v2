using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Rhino;
using Rhino.Collections;
using Rhino.Geometry;

namespace ProvingGround.Conduit.Utilities
{
    public static class Tiler
    {
        public static List<Rectangle3d> GridTiles(Rectangle3d bounds, double relativeInteriorPadding, double relativeExteriorPadding, int columns, int rows, PaddingSource paddingSource)
        {
            var tiles = new List<Rectangle3d>();
            Plane basePlane = new Plane(bounds.Corner(0), bounds.Plane.XAxis, bounds.Plane.YAxis);

            double padSource = paddingSource == PaddingSource.Width ? bounds.Width : bounds.Height;
            var ineriorPadding = relativeInteriorPadding * padSource;
            var exteriorPadding = relativeExteriorPadding * padSource;

            var cellWidth = ( bounds.Width - ( ineriorPadding * ( columns - 1 ) ) - ( exteriorPadding * 2 ) ) / columns;
            var cellHeight = ( bounds.Height - ( ineriorPadding * ( rows - 1 ) ) - ( exteriorPadding * 2 ) ) / rows;

            for (int i = 0; i < rows; i++)
            {
                double lowerLeftY = exteriorPadding + ( cellHeight * i ) + ( ineriorPadding * i );
                double upperRightY = exteriorPadding + ( cellHeight * ( i + 1 ) ) + ( ineriorPadding * i );
                for (int j = 0; j < columns; j++)
                {
                    double lowerLeftX = exteriorPadding + ( cellWidth * j ) + ( ineriorPadding * j );
                    double upperRightX = exteriorPadding + ( cellWidth * ( j + 1 ) ) + ( ineriorPadding * j );
                    tiles.Add(new Rectangle3d(basePlane, basePlane.PointAt(lowerLeftX, lowerLeftY, 0), basePlane.PointAt(upperRightX, upperRightY, 0)));
                }
            }

            return tiles;
        }

        public static List<Rectangle3d> HorizontalTiles(Rectangle3d bounds, List<double> relativeSizes, double relativeHeight, double relativeWidthPadding, double relativeHeightPadding)
        {
            if (relativeSizes == null)
            {
                throw new ArgumentNullException(nameof(relativeHeightPadding));
            }

            var tiles = new List<Rectangle3d>();
            var basePlane = new Plane(bounds.Corner(0), bounds.Plane.XAxis, bounds.Plane.YAxis);

            var available = bounds.Width - ( relativeWidthPadding * bounds.Width );

            double total = relativeSizes.Sum();
            double heightPadding = relativeHeightPadding * bounds.Height / 2;
            double widthPadding = relativeWidthPadding * bounds.Width / ( relativeSizes.Count + 1 );
            double height = ( bounds.Height * relativeHeight ) - ( relativeHeightPadding * bounds.Height / 2 );

            double currentXValue = 0;

            for (int i = 0; i < relativeSizes.Count; i++)
            {
                currentXValue += widthPadding;
                double nextX = currentXValue + ( relativeSizes[i] / total * available );
                tiles.Add(new Rectangle3d(
                    basePlane,
                    basePlane.PointAt(currentXValue, heightPadding, 0),
                    basePlane.PointAt(nextX, height, 0)));
                currentXValue = nextX;
            }

            return tiles;
        }

        public static List<Rectangle3d> IrregularTiles(Rectangle3d bounds, List<double> xRelativeSizes, List<double> yRelativeSizes, double relativeExteriorPadding, double relativeInteriorPadding, PaddingSource paddingSource)
        {
            if (xRelativeSizes == null)
            {
                throw new ArgumentNullException(nameof(xRelativeSizes));
            }

            if (yRelativeSizes == null)
            {
                throw new ArgumentNullException(nameof(yRelativeSizes));
            }

            var tileSet = new List<Rectangle3d>();

            var basePlane = new Plane(bounds.Corner(0), bounds.Plane.XAxis, bounds.Plane.YAxis);

            double xSum = xRelativeSizes.Sum();
            double ySum = yRelativeSizes.Sum();

            double padSource = paddingSource == PaddingSource.Width ? bounds.Width : bounds.Height;
            var interiorPadding = relativeInteriorPadding * padSource;
            var exteriorPadding = relativeExteriorPadding * padSource;

            double availableWidth = bounds.Width - ( 2 * exteriorPadding ) - ( ( xRelativeSizes.Count - 1 ) * interiorPadding );
            double availableHeight = bounds.Height - ( 2 * exteriorPadding ) - ( ( yRelativeSizes.Count - 1 ) * interiorPadding );

            double rowValue = 0;

            for (int i = 0; i < yRelativeSizes.Count; i++)
            {
                rowValue += i == 0 ? exteriorPadding : interiorPadding;
                double nextY = rowValue + ( yRelativeSizes[i] / ySum * availableHeight );

                double columnValue = 0;
                for (int j = 0; j < xRelativeSizes.Count; j++)
                {
                    columnValue += j == 0 ? exteriorPadding : interiorPadding;
                    double nextX = columnValue + ( xRelativeSizes[j] / xSum * availableWidth );

                    tileSet.Add(new Rectangle3d(
                        basePlane,
                        basePlane.PointAt(columnValue, rowValue, 0),
                        basePlane.PointAt(nextX, nextY, 0)));

                    columnValue = nextX;
                }

                rowValue = nextY;
            }

            return tileSet;
        }

        public static List<Rectangle3d> VerticalTiles(Rectangle3d bounds, List<double> relativeSizes, double relativeWidth, double relativeWidthPadding, double relativeHeightPadding)
        {
            if (relativeSizes == null)
            {
                throw new ArgumentNullException(nameof(relativeHeightPadding));
            }

            var tiles = new List<Rectangle3d>();
            var basePlane = new Plane(bounds.Corner(0), bounds.Plane.XAxis, bounds.Plane.YAxis);

            var available = bounds.Height - ( relativeHeightPadding * bounds.Height );

            double total = relativeSizes.Sum();
            double heightPadding = relativeHeightPadding * bounds.Height / ( relativeSizes.Count + 1 );
            double widthPadding = relativeWidthPadding * bounds.Width / 2;
            double width = ( bounds.Width * relativeWidth ) - ( relativeWidthPadding * bounds.Width / 2 );

            double currentYValue = 0;

            for (int i = 0; i < relativeSizes.Count; i++)
            {
                currentYValue += heightPadding;
                double nextY = currentYValue + ( relativeSizes[i] / total * available );
                tiles.Add(new Rectangle3d(
                    basePlane,
                    basePlane.PointAt(widthPadding, currentYValue, 0),
                    basePlane.PointAt(width, nextY, 0)));
                currentYValue = nextY;
            }

            return tiles;
        }
    }
}
