using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Rhino.Geometry;

namespace ProvingGround.Conduit.Utilities
{
    public class Interval2d
    {
        public Interval2d(Interval u, Interval v)
        {
            U = u;
            V = v;
        }

        public Interval2d(Rectangle3d rectangle)
        {
            U = new Interval(rectangle.Corner(0).X, rectangle.Corner(2).X);
            V = new Interval(rectangle.Corner(0).Y, rectangle.Corner(2).Y);
        }

        public Interval U { get; set; }

        public Interval V { get; set; }
    }
}
