using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProvingGround.Conduit.ObjectSettings
{
    public class TextSettings
    {
        public TextSettings(string font, double pixelHeight, double pixelPadding, bool bold, bool italic, Color color)
        {
            Font = font;
            PixelHeight = pixelHeight;
            PixelPadding = ( pixelHeight * 0.5 ) + pixelPadding;
            Bold = bold;
            Italic = italic;
            Color = color;
        }

        public bool Bold { get; set; }

        public Color Color { get; set; }

        public string Font { get; set; }

        public bool Italic { get; set; }

        public double PixelHeight { get; set; }

        public double PixelPadding { get; set; }
    }
}
