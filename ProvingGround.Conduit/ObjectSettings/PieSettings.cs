using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProvingGround.Conduit.ObjectSettings
{
    public class PieSettings
    {
        public PieSettings(PieType pieType, double innerRadius, double outerRadius, double labelHeight, double labelRadius, string labelFont, double labelOffset, Color labelColor, double rotation)
        {
            PieType = pieType;
            InnerRadius = innerRadius;
            OuterRadius = outerRadius;
            LabelHeight = labelHeight;
            LabelRadius = labelRadius;
            LabelFont = labelFont;
            LabelOffset = labelOffset;
            LabelColor = labelColor;
            Rotation = rotation;
        }

        public double InnerRadius { get; set; }

        public Color LabelColor { get; set; }

        public string LabelFont { get; set; }

        public double LabelHeight { get; set; }

        public double LabelOffset { get; set; }

        public double LabelRadius { get; set; }

        public double OuterRadius { get; set; }

        public PieType PieType { get; set; }

        public double Rotation { get; set; }
    }
}
