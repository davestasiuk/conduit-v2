using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProvingGround.Conduit.ObjectSettings;
using ProvingGround.Conduit.Utilities;
using Rhino.Display;
using Rhino.Geometry;
using Svg;

namespace ProvingGround.Conduit.DrawObjects
{
    public class DrawText : DrawObject
    {
        private readonly Distribution _distribution;
        private readonly Justification _justification;
        private readonly string[] _text;
        private readonly Interval2d _textBounds;
        private readonly TextSettings[] _textSettings;
        private double _heightPct;
        private Interval2d _normalizedBounds;
        private double[] _textHeightRatio;

        public DrawText(Rectangle3d textBounds, List<string> text, List<TextSettings> textSettings, Distribution distribution, Justification justification)
            : base("HUD Text")
        {
            Contract.Requires(text != null);
            Contract.Requires(textSettings != null);

            _textBounds = new Interval2d(textBounds);
            _text = text.ToArray();

            _textSettings = new TextSettings[text.Count];
            for (int i = 0; i < text.Count; i++)
            {
                _textSettings[i] = textSettings[i < textSettings.Count ? i : textSettings.Count - 1];
            }

            _distribution = distribution;
            _justification = justification;
        }

        public enum Distribution
        {
            Absolute,
            Relative,
        }

        public enum Justification
        {
            Left,
            Right,
            Center,
        }

        public double TextHeightUnits { get; }

        public double TextMaxWidth { get; }

        public double TextPaddingUnits { get; }

        public override void AddSVG(Interval2d svgBounds, SvgGroup svgGroup)
        {
            if (svgGroup == null)
            {
                throw new ArgumentNullException(nameof(svgGroup));
            }

            if (svgBounds == null)
            {
                throw new ArgumentNullException(nameof(svgBounds));
            }

            Point2d textPoint2d = Point2d.Unset;

            switch (_justification)
            {
                case Justification.Left:
                    textPoint2d = new Point2d(_normalizedBounds.U.Min, _normalizedBounds.V.Max);
                    break;

                case Justification.Right:
                    textPoint2d = new Point2d(_normalizedBounds.U.Max, _normalizedBounds.V.Max);
                    break;

                case Justification.Center:
                    textPoint2d = new Point2d(_normalizedBounds.U.Mid, _normalizedBounds.V.Max);
                    break;

                default:
                    break;
            }

            PointF textPoint = PointFOnSVG(textPoint2d, svgBounds);

            double totalHeight = 0;
            double pixels = _heightPct * svgBounds.V.Length;

            if (_distribution == Distribution.Relative)
            {
                int lastIndex = _textSettings.Length - 1;
                for (int i = 0; i < lastIndex; i++)
                {
                    totalHeight += _textSettings[i].PixelHeight;
                    totalHeight += _textSettings[i].PixelPadding;
                }

                totalHeight += _textSettings[lastIndex].PixelHeight;
            }

            for (int i = 0; i < _text.Length; i++)
            {
                double textHeight;
                double textPadding;
                if (_distribution == Distribution.Absolute)
                {
                    textHeight = _textSettings[i].PixelHeight;
                    textPadding = _textSettings[i].PixelPadding;
                }
                else
                {
                    textHeight = pixels * ( _textSettings[i].PixelHeight / totalHeight );
                    textPadding = pixels * ( _textSettings[i].PixelPadding / totalHeight );
                }

                float xShift = 0;

                if (_justification == Justification.Center)
                {
                    xShift = (float)( textHeight * _textHeightRatio[i] * 0.5 );
                }
                else if (_justification == Justification.Right)
                {
                    xShift = (float)( textHeight * _textHeightRatio[i] );
                }

                textPoint.Y = (float)( textPoint.Y + textHeight );
                var thisTextPoint = new PointF(textPoint.X - xShift, textPoint.Y);

                var text = new SvgText(_text[i])
                {
                    FontWeight = _textSettings[i].Bold ? SvgFontWeight.Bold : SvgFontWeight.Normal,
                    FontStyle = _textSettings[i].Italic ? SvgFontStyle.Italic : SvgFontStyle.Normal,
                    Font = _textSettings[i].Font,
                    FontSize = (float)( textHeight / 0.75 ),
                    Fill = new SvgColourServer(_textSettings[i].Color),
                };

                text.X = new SvgUnitCollection { thisTextPoint.X };
                text.Y = new SvgUnitCollection { thisTextPoint.Y };

                svgGroup.Children.Add(text);

                textPoint.Y = (float)( textPoint.Y + textPadding );
            }
        }

        public override void Draw(DrawEventArgs e, Plane drawPlane, UnitsPerPixel unitsPerPixel)
        {
            Point2d textPoint2d = Point2d.Unset;

            switch (_justification)
            {
                case Justification.Left:
                    textPoint2d = new Point2d(_normalizedBounds.U.Min, _normalizedBounds.V.Max);
                    break;

                case Justification.Right:
                    textPoint2d = new Point2d(_normalizedBounds.U.Max, _normalizedBounds.V.Max);
                    break;

                case Justification.Center:
                    textPoint2d = new Point2d(_normalizedBounds.U.Mid, _normalizedBounds.V.Max);
                    break;

                default:
                    break;
            }

            Point3d textPoint = PointOnHUD(e, drawPlane, unitsPerPixel, textPoint2d);

            double totalHeight = 0;
            double pixels = _heightPct * e.Viewport.Bounds.Height;

            if (_distribution == Distribution.Relative)
            {
                int lastIndex = _textSettings.Length - 1;
                for (int i = 0; i < lastIndex; i++)
                {
                    totalHeight += _textSettings[i].PixelHeight;
                    totalHeight += _textSettings[i].PixelPadding;
                }

                totalHeight += _textSettings[lastIndex].PixelHeight;
            }

            for (int i = 0; i < _text.Length; i++)
            {
                double textHeight;
                double textPadding;
                if (_distribution == Distribution.Absolute)
                {
                    textHeight = unitsPerPixel.Height * _textSettings[i].PixelHeight;
                    textPadding = unitsPerPixel.Height * _textSettings[i].PixelPadding;
                }
                else
                {
                    textHeight = unitsPerPixel.Height * ( pixels * ( _textSettings[i].PixelHeight / totalHeight ) );
                    textPadding = unitsPerPixel.Height * ( pixels * ( _textSettings[i].PixelPadding / totalHeight ) );
                }

                textPoint -= drawPlane.YAxis * textHeight;
                var textPlane = new Plane(textPoint, drawPlane.XAxis, drawPlane.YAxis);

                if (_justification == Justification.Center)
                {
                    Vector3d centerVector = -drawPlane.XAxis * textHeight * _textHeightRatio[i] * 0.5;
                    textPlane.Translate(centerVector);
                }
                else if (_justification == Justification.Right)
                {
                    Vector3d rightVector = -drawPlane.XAxis * textHeight * _textHeightRatio[i];
                    textPlane.Translate(rightVector);
                }

                using (var text = new Text3d(_text[i], textPlane, textHeight)
                {
                    Bold = _textSettings[i].Bold,
                    Italic = _textSettings[i].Italic,
                    FontFace = _textSettings[i].Font,
                })
                {
                    e.Display.Draw3dText(text, _textSettings[i].Color);
                }

                textPoint -= drawPlane.YAxis * textPadding;
            }
        }

        public override void SetupWithViewBounds(Interval2d viewBounds)
        {
            Point2d lowerLeft = NormalizedPointInBounds(new Point2d(_textBounds.U.Min, _textBounds.V.Min), viewBounds);
            Point2d upperRight = NormalizedPointInBounds(new Point2d(_textBounds.U.Max, _textBounds.V.Max), viewBounds);

            _normalizedBounds = new Interval2d(
                new Interval(lowerLeft.X, upperRight.X),
                new Interval(lowerLeft.Y, upperRight.Y));

            _textHeightRatio = new double[_text.Length];

            for (int i = 0; i < _text.Length; i++)
            {
                using (var measureText = new Text3d(_text[i], Plane.WorldXY, 1.0)
                {
                    Bold = _textSettings[i].Bold,
                    Italic = _textSettings[i].Italic,
                    FontFace = _textSettings[i].Font,
                })
                {
                    var measureTextBounds = new Box(measureText.BoundingBox);
                    double textHeightRatio = measureTextBounds.X.Max;
                    _textHeightRatio[i] = textHeightRatio;
                }
            }

            if (_distribution == Distribution.Relative)
            {
                _heightPct = _textBounds.V.Length / viewBounds.V.Length;
            }
        }
    }
}
