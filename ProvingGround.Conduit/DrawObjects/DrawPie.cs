using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProvingGround.Conduit.ObjectSettings;
using ProvingGround.Conduit.Utilities;
using Rhino.Display;
using Rhino.Geometry;
using Svg;
using Svg.Pathing;

namespace ProvingGround.Conduit.DrawObjects
{
    public class DrawPie : DrawObject
    {
        private const double _degree = Math.PI * 2 / 360;

        private readonly double[] _angles;

        // the start angle, the length of each segment in radians, and the number of segments in the mesh
        private readonly Tuple<double, double, int>[] _arcDefinitions;

        private readonly Color[] _colors;
        private readonly double[] _labelLocations;

        private readonly string[] _labels;
        private readonly Interval2d _pieBounds;
        private readonly PieSettings _pieSettings;
        private readonly Mesh[] _slices;

        // adjusted radii reflect radius relative to overall view bounds (as a percent)

        private double _adjustedInnerRadius;
        private double _adjustedLabelHeight;
        private double _adjustedLabelRadius;
        private double _adjustedOuterRadius;

        private ObjectAspect _aspect;
        private Point2d _center;
        private double[] _labelHeightRatio;

        public DrawPie(Rectangle3d pieBounds, PieSettings pieSettings, List<double> values, List<Color> colors, List<string> labels)
            : base("HUD Pie")
        {
            Contract.Requires(pieSettings != null);
            Contract.Requires(values != null);
            Contract.Requires(colors != null);
            Contract.Requires(labels != null);

            _pieBounds = new Interval2d(pieBounds);
            _pieSettings = pieSettings;

            double currentRotation = _pieSettings.Rotation;
            double sum = values.Sum();

            _arcDefinitions = new Tuple<double, double, int>[values.Count];
            _labelLocations = new double[values.Count];
            _slices = new Mesh[values.Count];
            _angles = new double[values.Count];

            for (int i = 0; i < values.Count; i++)
            {
                double pct = values[i] / sum;
                _angles[i] = pct * 360.0;

                double thisLength = pct * 2 * Math.PI;
                int segmentCount = (int)Math.Ceiling(thisLength / _degree);
                double segmentLength = thisLength / segmentCount;

                _arcDefinitions[i] = Tuple.Create(currentRotation, segmentLength, segmentCount);
                _labelLocations[i] = currentRotation + ( thisLength * 0.5 );

                var slice = new Mesh();
                double segmentAdd = currentRotation;
                var origin = new Plane(pieBounds.Center, Vector3d.XAxis, Vector3d.YAxis);

                if (_pieSettings.PieType == PieType.Pie)
                {
                    slice.Vertices.Add(pieBounds.Center);
                    slice.Vertices.Add(PolarPoint(origin, 1, segmentAdd));

                    for (int j = 0; j < segmentCount; j++)
                    {
                        segmentAdd += segmentLength;
                        slice.Vertices.Add(PolarPoint(origin, 1, segmentAdd));
                        slice.Faces.AddFace(0, j + 2, j + 1);
                    }
                }
                else
                {
                    slice.Vertices.Add(PolarPoint(origin, 0.5, segmentAdd));
                    slice.Vertices.Add(PolarPoint(origin, 1.0, segmentAdd));

                    for (int j = 0; j < segmentCount; j++)
                    {
                        int index = j * 2;
                        segmentAdd += segmentLength;
                        slice.Vertices.Add(PolarPoint(origin, 0.5, segmentAdd));
                        slice.Vertices.Add(PolarPoint(origin, 1.0, segmentAdd));
                        slice.Faces.AddFace(index + 1, index, index + 2, index + 3);
                    }
                }

                _slices[i] = slice;
                currentRotation += thisLength;
            }

            _colors = new Color[colors.Count];
            for (int i = 0; i < colors.Count; i++)
            {
                _colors[i] = colors[i];
                _slices[i].VertexColors.Clear();

                for (int j = 0; j < _slices[i].Vertices.Count; j++)
                {
                    _slices[i].VertexColors.Add(colors[i]);
                }
            }

            _labels = new string[labels.Count];
            for (int i = 0; i < labels.Count; i++)
            {
                _labels[i] = labels[i];
            }
        }

        public override void AddSVG(Interval2d svgBounds, SvgGroup svgGroup)
        {
            if (svgBounds == null || svgGroup == null)
            {
                return;
            }

            double outerRadius = _aspect == ObjectAspect.Width ?
                _adjustedOuterRadius * svgBounds.U.Length :
                _adjustedOuterRadius * svgBounds.V.Length;

            double innerRadius = _pieSettings.PieType == PieType.Pie ? 0.0 :
                _aspect == ObjectAspect.Width ?
                _adjustedInnerRadius * svgBounds.U.Length :
                _adjustedInnerRadius * svgBounds.V.Length;

            var center = PointFOnSVG(_center, svgBounds);

            for (int i = 0; i < _arcDefinitions.Length; i++)
            {
                var path = new SvgPath
                {
                    PathData = new SvgPathSegmentList(),
                };

                path.StrokeWidth = 0;
                path.Fill = new SvgColourServer(_colors[i]);

                double start = _arcDefinitions[i].Item1;
                double end = _arcDefinitions[i].Item1 + ( _arcDefinitions[i].Item2 * _arcDefinitions[i].Item3 );

                var outerStart = PolarPointFInAspect(_adjustedOuterRadius, start, svgBounds, _aspect);
                outerStart.X += center.X;
                outerStart.Y += center.Y;

                var outerEnd = PolarPointFInAspect(_adjustedOuterRadius, end, svgBounds, _aspect);
                outerEnd.X += center.X;
                outerEnd.Y += center.Y;

                path.PathData.Add(new SvgMoveToSegment(outerStart));
                path.PathData.Add(new SvgArcSegment(
                    outerStart,
                    (float)outerRadius,
                    (float)outerRadius,
                    (float)_angles[i],
                    _angles[i] < 180 ? SvgArcSize.Small : SvgArcSize.Large,
                    SvgArcSweep.Negative,
                    outerEnd));

                if (_pieSettings.PieType == PieType.Pie)
                {
                    path.PathData.Add(new SvgLineSegment(outerEnd, center));
                    path.PathData.Add(new SvgLineSegment(center, outerStart));
                }
                else
                {
                    var innerStart = PolarPointFInAspect(_adjustedInnerRadius, start, svgBounds, _aspect);
                    innerStart.X += center.X;
                    innerStart.Y += center.Y;

                    var innerEnd = PolarPointFInAspect(_adjustedInnerRadius, end, svgBounds, _aspect);
                    innerEnd.X += center.X;
                    innerEnd.Y += center.Y;

                    path.PathData.Add(new SvgLineSegment(outerEnd, innerEnd));
                    path.PathData.Add(new SvgArcSegment(
                        innerEnd,
                        (float)innerRadius,
                        (float)innerRadius,
                        (float)_angles[i],
                        _angles[i] < 180 ? SvgArcSize.Small : SvgArcSize.Large,
                        SvgArcSweep.Positive,
                        innerStart));
                    path.PathData.Add(new SvgLineSegment(innerStart, outerStart));
                }

                svgGroup.Children.Add(path);
            }

            if (_labels.Length > 0)
            {
                double labelRadius = _aspect == ObjectAspect.Width ?
                _adjustedLabelRadius * svgBounds.U.Length :
                _adjustedLabelRadius * svgBounds.V.Length;

                double textHeight = _aspect == ObjectAspect.Width ?
                _adjustedLabelHeight * svgBounds.U.Length :
                _adjustedLabelHeight * svgBounds.V.Length;

                float adjustedTextHeight = (float)( textHeight / 0.75 );

                for (int i = 0; i < _labels.Length; i++)
                {
                    double textWidth = textHeight * _labelHeightRatio[i];

                    var labelPoint = PolarPointFInAspect(_adjustedLabelRadius, _labelLocations[i], svgBounds, _aspect);
                    labelPoint.X += center.X;
                    labelPoint.Y += center.Y;

                    double textOffset = textWidth * 0.5;
                    if (_pieSettings.LabelOffset > 0)
                    {
                        textOffset -= Math.Cos(_labelLocations[i]) * _pieSettings.LabelOffset;
                    }

                    labelPoint.X -= (float)textOffset;
                    labelPoint.Y += (float)( textHeight * 0.5 );

                    var text = new SvgText(_labels[i])
                    {
                        Font = _pieSettings.LabelFont,
                        FontSize = adjustedTextHeight,
                        Fill = new SvgColourServer(_pieSettings.LabelColor),
                    };

                    text.X = new SvgUnitCollection { labelPoint.X };
                    text.Y = new SvgUnitCollection { labelPoint.Y };

                    svgGroup.Children.Add(text);
                }
            }
        }

        public override void Draw(DrawEventArgs e, Plane drawPlane, UnitsPerPixel unitsPerPixel)
        {
            if (unitsPerPixel == null)
            {
                return;
            }

            if (e == null)
            {
                return;
            }

            double outerRadius = _aspect == ObjectAspect.Width ?
                _adjustedOuterRadius * e.Viewport.Bounds.Width * unitsPerPixel.Width :
                _adjustedOuterRadius * e.Viewport.Bounds.Height * unitsPerPixel.Height;

            double innerRadius = _pieSettings.PieType == PieType.Pie ? 0.0 :
                _aspect == ObjectAspect.Width ?
                _adjustedInnerRadius * e.Viewport.Bounds.Width * unitsPerPixel.Width :
                _adjustedInnerRadius * e.Viewport.Bounds.Height * unitsPerPixel.Height;

            Point3d center = PointOnHUD(e, drawPlane, unitsPerPixel, _center);
            var piePlane = new Plane(center, drawPlane.XAxis, drawPlane.YAxis);

            for (int i = 0; i < _arcDefinitions.Length; i++)
            {
                double start = _arcDefinitions[i].Item1;
                double segmentLength = _arcDefinitions[i].Item2;
                double segmentCount = _arcDefinitions[i].Item3;

                if (_pieSettings.PieType == PieType.Pie)
                {
                    _slices[i].Vertices.SetVertex(0, center);

                    for (int j = 0; j < segmentCount + 1; j++)
                    {
                        _slices[i].Vertices.SetVertex(j + 1, PolarPoint(piePlane, outerRadius, start));
                        start += segmentLength;
                    }
                }
                else
                {
                    for (int j = 0; j < segmentCount + 1; j++)
                    {
                        int index = j * 2;
                        _slices[i].Vertices.SetVertex(index, PolarPoint(piePlane, innerRadius, start));
                        _slices[i].Vertices.SetVertex(index + 1, PolarPoint(piePlane, outerRadius, start));
                        start += segmentLength;
                    }
                }
            }

            for (int i = 0; i < _slices.Length; i++)
            {
                e.Display.DrawMeshFalseColors(_slices[i]);
            }

            if (_labels.Length > 0)
            {
                double labelRadius = _aspect == ObjectAspect.Width ?
                _adjustedLabelRadius * e.Viewport.Bounds.Width * unitsPerPixel.Width :
                _adjustedLabelRadius * e.Viewport.Bounds.Height * unitsPerPixel.Height;

                double textHeight = _aspect == ObjectAspect.Width ?
                _adjustedLabelHeight * e.Viewport.Bounds.Width * unitsPerPixel.Width :
                _adjustedLabelHeight * e.Viewport.Bounds.Height * unitsPerPixel.Height;

                for (int i = 0; i < _labels.Length; i++)
                {
                    double textWidth = textHeight * _labelHeightRatio[i];
                    Point3d labelOrigin = PolarPoint(piePlane, labelRadius, _labelLocations[i]);
                    double textOffset = textWidth * 0.5;
                    if (_pieSettings.LabelOffset > 0)
                    {
                        textOffset -= Math.Cos(_labelLocations[i]) * _pieSettings.LabelOffset;
                    }

                    labelOrigin += ( drawPlane.XAxis * -textOffset ) + ( drawPlane.YAxis * -textHeight * 0.5 );
                    var labelPlane = new Plane(labelOrigin, drawPlane.XAxis, drawPlane.YAxis);
                    e.Display.Draw3dText(_labels[i], _pieSettings.LabelColor, labelPlane, textHeight, _pieSettings.LabelFont);
                }
            }
        }

        public override void SetupWithViewBounds(Interval2d viewBounds)
        {
            _center = NormalizedPointInBounds(new Point2d(_pieBounds.U.Mid, _pieBounds.V.Mid), viewBounds);

            double uRadius = _pieBounds.U.Max - _pieBounds.U.Mid;
            double vRadius = _pieBounds.V.Max - _pieBounds.V.Mid;

            double minRadius;
            if (uRadius <= vRadius)
            {
                minRadius = uRadius / viewBounds.U.Length;
                _aspect = ObjectAspect.Width;
            }
            else
            {
                minRadius = vRadius / viewBounds.V.Length;
                _aspect = ObjectAspect.Height;
            }

            _adjustedInnerRadius = _pieSettings.InnerRadius * minRadius;
            _adjustedOuterRadius = _pieSettings.OuterRadius * minRadius;
            _adjustedLabelRadius = _pieSettings.LabelRadius * minRadius;
            _adjustedLabelHeight = _pieSettings.LabelHeight * minRadius * 2;

            _labelHeightRatio = new double[_labels.Length];

            for (int i = 0; i < _labels.Length; i++)
            {
                using (var measureText = new Text3d(_labels[i], Plane.WorldXY, 1.0)
                {
                    FontFace = _pieSettings.LabelFont,
                })
                {
                    var measureTextBounds = new Box(measureText.BoundingBox);
                    double textHeightRatio = measureTextBounds.X.Max;
                    _labelHeightRatio[i] = textHeightRatio;
                }
            }
        }

        private Point3d PolarPoint(Plane drawPlane, double radius, double angle)
        {
            return drawPlane.PointAt(radius * Math.Cos(angle), radius * Math.Sin(angle));
        }

        private PointF PolarPointFInAspect(double radius, double angle, Interval2d bounds, ObjectAspect aspect)
        {
            if (bounds == null)
            {
                throw new ArgumentNullException(nameof(bounds));
            }

            double length = aspect == ObjectAspect.Height ? bounds.V.Length : bounds.U.Length;

            return new PointF(
                (float)( radius * Math.Cos(angle) * length ),
                (float)( radius * Math.Sin(angle) * length * -1 ));
        }
    }
}
