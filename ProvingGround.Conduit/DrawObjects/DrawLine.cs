using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProvingGround.Conduit.Utilities;
using Rhino.Display;
using Rhino.Geometry;
using Svg;

namespace ProvingGround.Conduit.DrawObjects
{
    /// <summary>
    /// A drawing object for creating lines in the HUD.
    /// </summary>
    /// <seealso cref="ProvingGround.Conduit.DrawObjects.DrawObject" />
    public class DrawLine : DrawObject
    {
        private readonly Color _color;
        private readonly Line _line;
        private readonly int _thickness;
        private Point2d[] _points;

        /// <summary>
        /// Initializes a new instance of the <see cref="DrawLine"/> class.
        /// </summary>
        /// <param name="line">The line to be drawn.</param>
        /// <param name="thickness">The line's specified thickness.</param>
        /// <param name="color">The line's specified color.</param>
        public DrawLine(Line line, int thickness, Color color)
            : base("HUD Line")
        {
            _line = line;
            _thickness = thickness;
            _color = color;
        }

        public override void AddSVG(Interval2d svgBounds, SvgGroup svgGroup)
        {
            if (svgGroup == null)
            {
                return;
            }

            Point2d from = PointOnSVG(_points[0], svgBounds);
            Point2d to = PointOnSVG(_points[1], svgBounds);

            svgGroup.Children.Add(
                new SvgLine
                {
                    StartX = new SvgUnit((float)from.X),
                    StartY = new SvgUnit((float)from.Y),
                    EndX = new SvgUnit((float)to.X),
                    EndY = new SvgUnit((float)to.Y),
                    Stroke = new SvgColourServer(_color),
                    StrokeWidth = _thickness,
                });
        }

        /// <summary>
        /// Method called for drawing this instance in the HUD.
        /// </summary>
        /// <param name="e">The <see cref="DrawEventArgs" /> instance containing the event data.</param>
        /// <param name="drawPlane">The draw plane at the lower left corner of the screen.</param>
        /// <param name="unitsPerPixel">The units per pixel conversion ratios.</param>
        public override void Draw(DrawEventArgs e, Plane drawPlane, UnitsPerPixel unitsPerPixel)
        {
            if (e == null)
            {
                throw new ArgumentNullException(nameof(e));
            }

            var drawLine = new Line(
                PointOnHUD(e, drawPlane, unitsPerPixel, _points[0]),
                PointOnHUD(e, drawPlane, unitsPerPixel, _points[1]));

            e.Display.DrawLine(drawLine, _color, _thickness);
        }

        /// <summary>
        /// Method called to prepare this class within the specified view bounds.
        /// </summary>
        /// <param name="viewBounds">The view bounds.</param>
        public override void SetupWithViewBounds(Interval2d viewBounds)
        {
            _points = new Point2d[]
            {
                NormalizedPointInBounds(_line.From, viewBounds),
                NormalizedPointInBounds(_line.To, viewBounds),
            };
        }
    }
}
