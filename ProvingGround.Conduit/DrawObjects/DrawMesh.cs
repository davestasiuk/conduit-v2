using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProvingGround.Conduit.Utilities;
using Rhino.Display;
using Rhino.Geometry;
using Svg;
using Svg.Pathing;

namespace ProvingGround.Conduit.DrawObjects
{
    /// <summary>
    /// A drawing object for drawing a colored mesh in the HUD.
    /// </summary>
    /// <seealso cref="ProvingGround.Conduit.DrawObjects.DrawObject" />
    public class DrawMesh : DrawObject
    {
        private readonly Color _color;
        private readonly Mesh _mesh;
        private readonly Point3d[] _originalVertices;
        private List<Point2d> _vertices2d;

        /// <summary>
        /// Initializes a new instance of the <see cref="DrawMesh"/> class.
        /// </summary>
        /// <param name="mesh">The mesh to draw.</param>
        /// <param name="color">The color of the mesh.</param>
        public DrawMesh(Mesh mesh, Color color)
            : base("HUD Mesh")
        {
            Contract.Requires(mesh != null);

            _mesh = mesh;
            _color = color;

            _mesh.VertexColors.Clear();
            for (int i = 0; i < mesh.Vertices.Count; i++)
            {
                _mesh.VertexColors.Add(color);
            }

            _originalVertices = _mesh.Vertices.ToPoint3dArray();
        }

        public override void AddSVG(Interval2d svgBounds, SvgGroup svgGroup)
        {
            if (svgGroup != null)
            {
                using (var svgMesh = new Mesh())
                {
                    svgMesh.Append(_mesh);
                    for (int i = 0; i < svgMesh.Vertices.Count; i++)
                    {
                        svgMesh.Vertices.SetVertex(i, _vertices2d[i].X, _vertices2d[i].Y, 0);
                    }

                    Polyline[] edges = svgMesh.GetNakedEdges();

                    for (int i = 0; i < edges.Length; i++)
                    {
                        Polyline edge = edges[i];

                        var path = new SvgPath
                        {
                            PathData = new SvgPathSegmentList(),
                        };

                        path.PathData.Add(new SvgMoveToSegment(PointFOnSVG(edge[0], svgBounds)));

                        for (int j = 0; j < edge.Count; j++)
                        {
                            int next = j == edge.Count - 1 ? 0 : j + 1;
                            path.PathData.Add(new SvgLineSegment(
                                PointFOnSVG(edge[j], svgBounds),
                                PointFOnSVG(edge[next], svgBounds)));
                        }

                        path.StrokeWidth = 0;
                        path.Fill = new SvgColourServer(_color);

                        svgGroup.Children.Add(path);
                    }
                }
            }
        }

        /// <summary>
        /// Method called for drawing this instance in the HUD.
        /// </summary>
        /// <param name="e">The <see cref="DrawEventArgs" /> instance containing the event data.</param>
        /// <param name="drawPlane">The draw plane at the lower left corner of the screen.</param>
        /// <param name="unitsPerPixel">The units per pixel conversion ratios.</param>
        public override void Draw(DrawEventArgs e, Plane drawPlane, UnitsPerPixel unitsPerPixel)
        {
            if (e == null)
            {
                throw new ArgumentNullException(nameof(e));
            }

            for (int i = 0; i < _vertices2d.Count; i++)
            {
                Point3d vertex = PointOnHUD(e, drawPlane, unitsPerPixel, _vertices2d[i]);
                _mesh.Vertices.SetVertex(i, vertex);
            }

            e.Display.DrawMeshFalseColors(_mesh);
        }

        /// <summary>
        /// Method called to prepare this class within the specified view bounds.
        /// </summary>
        /// <param name="viewBounds">The view bounds.</param>
        public override void SetupWithViewBounds(Interval2d viewBounds)
        {
            _vertices2d = new List<Point2d>();

            for (int i = 0; i < _originalVertices.Length; i++)
            {
                _vertices2d.Add(NormalizedPointInBounds(_originalVertices[i], viewBounds));
            }
        }
    }
}
