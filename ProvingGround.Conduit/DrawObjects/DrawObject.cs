using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProvingGround.Conduit.Utilities;
using Rhino.Display;
using Rhino.Geometry;
using Svg;

namespace ProvingGround.Conduit.DrawObjects
{
    /// <summary>
    /// The base class for different types of drawing objects in the HUD.
    /// </summary>
    public abstract class DrawObject
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DrawObject"/> class.
        /// </summary>
        /// <param name="objectType">Type of the object.</param>
        public DrawObject(string objectType)
        {
            ObjectType = objectType;
        }

        /// <summary>
        /// The human-readable name of this object (for previewing on the GH canvas).
        /// </summary>
        /// <value>
        /// The string.
        /// </value>
        public string ObjectType { get; private set; }

        /// <summary>
        /// Returns the relative 2d location of a point within a specified 2d interval.
        /// </summary>
        /// <param name="point">The 2d point.</param>
        /// <param name="bounds">The bounds.</param>
        /// <returns>The 2d Point.</returns>
        public static Point2d NormalizedPointInBounds(Point3d point, Interval2d bounds)
        {
            if (bounds == null)
            {
                throw new ArgumentNullException(nameof(bounds));
            }

            return new Point2d(
              bounds.U.NormalizedParameterAt(point.X),
              bounds.V.NormalizedParameterAt(point.Y));
        }

        /// <summary>
        /// Returns the relative 2d location of a point within a specified 2d interval.
        /// </summary>
        /// <param name="point">The 2d point.</param>
        /// <param name="bounds">The bounds.</param>
        /// <returns>The 2d Point.</returns>
        public static Point2d NormalizedPointInBounds(Point2d point, Interval2d bounds)
        {
            if (bounds == null)
            {
                throw new ArgumentNullException(nameof(bounds));
            }

            return new Point2d(
              bounds.U.NormalizedParameterAt(point.X),
              bounds.V.NormalizedParameterAt(point.Y));
        }

        /// <summary>
        /// Get a PointF relative to the SVG bounds.
        /// </summary>
        /// <param name="point">Input point.</param>
        /// <param name="bounds">Svg bounds.</param>
        public static PointF PointFOnSVG(Point2d point, Interval2d bounds)
        {
            if (bounds == null)
            {
                throw new ArgumentNullException(nameof(bounds));
            }

            return new PointF(
                (float)( point.X * bounds.U.Length ),
                (float)( bounds.V.Length - ( bounds.V.Length * point.Y ) ));
        }

        /// <summary>
        /// Get a PointF relative to the SVG bounds.
        /// </summary>
        /// <param name="point">Input point.</param>
        /// <param name="bounds">Svg bounds.</param>
        public static PointF PointFOnSVG(Point3d point, Interval2d bounds)
        {
            if (bounds == null)
            {
                throw new ArgumentNullException(nameof(bounds));
            }

            return new PointF(
                (float)( point.X * bounds.U.Length ),
                (float)( bounds.V.Length - ( bounds.V.Length * point.Y ) ));
        }

        /// <summary>
        /// Returns the 3d point on the HUD based on the specified Viewport.
        /// </summary>
        /// <param name="e">The <see cref="DrawEventArgs"/> instance containing the event data.</param>
        /// <param name="drawPlane">The draw plane at the lower left corner of the screen.</param>
        /// <param name="unitsPerPixel">The units per pixel conversion ratios.</param>
        /// <param name="point">The 2d point in relative screen location.</param>
        /// <param name="depth">The relative pixel depth at which the point should be drawn.</param>
        /// <returns>The 3d Point in the HUD.</returns>
        public static Point3d PointOnHUD(DrawEventArgs e, Plane drawPlane, UnitsPerPixel unitsPerPixel, Point2d point, double depth)
        {
            if (unitsPerPixel == null)
            {
                throw new ArgumentNullException(nameof(unitsPerPixel));
            }

            if (e == null)
            {
                throw new ArgumentNullException(nameof(e));
            }

            return drawPlane.PointAt(
              unitsPerPixel.Width * ( point.X * e.Display.Viewport.Size.Width ),
              unitsPerPixel.Height * ( point.Y * e.Display.Viewport.Size.Height ),
              -unitsPerPixel.Width * depth);
        }

        /// <summary>
        /// Returns the 3d point on the HUD based on the specified Viewport.
        /// </summary>
        /// <param name="e">The <see cref="DrawEventArgs"/> instance containing the event data.</param>
        /// <param name="drawPlane">The draw plane at the lower left corner of the screen.</param>
        /// <param name="unitsPerPixel">The units per pixel conversion ratios.</param>
        /// <param name="point">The 2d point in relative screen location.</param>
        /// <returns>The 3d Point in the HUD.</returns>
        public static Point3d PointOnHUD(DrawEventArgs e, Plane drawPlane, UnitsPerPixel unitsPerPixel, Point2d point)
        {
            if (unitsPerPixel == null)
            {
                throw new ArgumentNullException(nameof(unitsPerPixel));
            }

            if (e == null)
            {
                throw new ArgumentNullException(nameof(e));
            }

            return drawPlane.PointAt(
              unitsPerPixel.Width * ( point.X * e.Display.Viewport.Size.Width ),
              unitsPerPixel.Height * ( point.Y * e.Display.Viewport.Size.Height ),
              0);
        }

        /// <summary>
        /// Returns the 3d point on the HUD based on the specified Viewport.
        /// </summary>
        /// <param name="e">The <see cref="DrawEventArgs"/> instance containing the event data.</param>
        /// <param name="drawPlane">The draw plane at the lower left corner of the screen.</param>
        /// <param name="unitsPerPixel">The units per pixel conversion ratios.</param>
        /// <param name="point">The 3d point in relative screen location.</param>
        /// <returns>The 3d Point in the HUD.</returns>
        public static Point3d PointOnHUD(DrawEventArgs e, Plane drawPlane, UnitsPerPixel unitsPerPixel, Point3d point)
        {
            if (unitsPerPixel == null)
            {
                throw new ArgumentNullException(nameof(unitsPerPixel));
            }

            if (e == null)
            {
                throw new ArgumentNullException(nameof(e));
            }

            return drawPlane.PointAt(
              unitsPerPixel.Width * ( point.X * e.Display.Viewport.Size.Width ),
              unitsPerPixel.Height * ( point.Y * e.Display.Viewport.Size.Height ),
              0);
        }

        /// <summary>
        /// Get a 2d point relative to the SVG bounds.
        /// </summary>
        /// <param name="point">Input point.</param>
        /// <param name="bounds">Svg bounds.</param>
        public static Point2d PointOnSVG(Point2d point, Interval2d bounds)
        {
            if (bounds == null)
            {
                throw new ArgumentNullException(nameof(bounds));
            }

            return new Point2d(
                point.X * bounds.U.Length,
                bounds.V.Length - ( bounds.V.Length * point.Y ));
        }

        /// <summary>
        /// Get a 2d point relative to the SVG bounds.
        /// </summary>
        /// <param name="point">Input point.</param>
        /// <param name="bounds">Svg bounds.</param>
        public static Point2d PointOnSVG(Point3d point, Interval2d bounds)
        {
            if (bounds == null)
            {
                throw new ArgumentNullException(nameof(bounds));
            }

            return new Point2d(
                point.X * bounds.U.Length,
                bounds.V.Length - ( bounds.V.Length * point.Y ));
        }

        /// <summary>
        /// Method called when authoring an SVG from the HUD.
        /// </summary>
        /// <param name="svgBounds">A 2d interval defining the bounding condition of the SVG document.</param>
        /// <param name="svgGroup">The Svg Object group that the method should append new Svg Elements to.</param>
        public abstract void AddSVG(Interval2d svgBounds, SvgGroup svgGroup);

        /// <summary>
        /// Method called for drawing this instance in the HUD.
        /// </summary>
        /// <param name="e">The <see cref="DrawEventArgs"/> instance containing the event data.</param>
        /// <param name="drawPlane">The draw plane at the lower left corner of the screen.</param>
        /// <param name="unitsPerPixel">The units per pixel conversion ratios.</param>
        public abstract void Draw(DrawEventArgs e, Plane drawPlane, UnitsPerPixel unitsPerPixel);

        /// <summary>
        /// Method called to prepare this class within the specified view bounds.
        /// </summary>
        /// <param name="viewBounds">The view bounds.</param>
        public abstract void SetupWithViewBounds(Interval2d viewBounds);
    }
}
