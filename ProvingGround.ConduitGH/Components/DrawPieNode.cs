using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Grasshopper.Kernel;
using ProvingGround.Conduit.DrawObjects;
using ProvingGround.Conduit.ObjectSettings;
using ProvingGround.ConduitGH.Settings;
using Rhino.Geometry;

namespace ProvingGround.ConduitGH.Components
{
    public class DrawPieNode : GH_Component
    {
        public DrawPieNode()
            : base("Draw Pie", "HUD Pie", "Draw a pie or donut chart in the HUD", "Proving Ground", "HUD v2")
        {
        }

        public override Guid ComponentGuid => new Guid("ad0062f3-51ff-4629-b4fc-b71261205929");

        public override GH_Exposure Exposure => GH_Exposure.tertiary;

        protected override Bitmap Icon => Properties.Resources.PG_Conduit_Pie;

        public override void DocumentContextChanged(GH_Document document, GH_DocumentContext context)
        {
            if (context == GH_DocumentContext.Open)
            {
                var pieSettingsParameter = Params.Input[4] as ParamPieSettings;
                if (pieSettingsParameter.Sources.Count == 0)
                {
                    SetDefaultPieSettings(pieSettingsParameter);
                }
            }

            base.DocumentContextChanged(document, context);
        }

        protected override void RegisterInputParams(GH_InputParamManager pManager)
        {
            if (pManager == null)
            {
                throw new ArgumentNullException(nameof(pManager));
            }

            pManager.AddRectangleParameter("Bounds", "Bounds", "Line in the drawing space", GH_ParamAccess.item);
            pManager.AddNumberParameter("Values", "Values", "Numeric values for the pie chart", GH_ParamAccess.list);
            pManager.AddColourParameter("Colors", "Colors", "Colors for the pie segments", GH_ParamAccess.list);
            pManager.AddTextParameter("Labels", "Labels", "Descriptive labels for the pie segments", GH_ParamAccess.list);
            pManager[3].Optional = true;

            var pieSettingsParameter = new ParamPieSettings { Name = "Pie settings", NickName = "Settings", Description = "Pie settings", Access = GH_ParamAccess.item };
            SetDefaultPieSettings(pieSettingsParameter);
            pManager.AddParameter(pieSettingsParameter);
        }

        protected override void RegisterOutputParams(GH_OutputParamManager pManager)
        {
            if (pManager == null)
            {
                throw new ArgumentNullException(nameof(pManager));
            }

            pManager.AddParameter(new ParamDrawObject { Name = "HUD Drawing Object", NickName = "DrawObj", Access = GH_ParamAccess.item });
        }

        protected override void SolveInstance(IGH_DataAccess dA)
        {
            if (dA == null)
            {
                throw new ArgumentNullException(nameof(dA));
            }

            Rectangle3d bounds = Rectangle3d.Unset;
            dA.GetData(0, ref bounds);

            var values = new List<double>();
            dA.GetDataList(1, values);

            var colors = new List<Color>();
            dA.GetDataList(2, colors);

            var labels = new List<string>();
            dA.GetDataList(3, labels);

            GHPieSettings ghPieSettings = null;
            dA.GetData(4, ref ghPieSettings);

            var drawPie = new DrawPie(bounds, ghPieSettings.Value, values, colors, labels);
            var drawObject = new GHDrawObject(drawPie);

            dA.SetData(0, drawObject);
        }

        private void SetDefaultPieSettings(ParamPieSettings pieSettingsParameter)
        {
            var defaultFont = new GHPieSettings(new PieSettings(PieType.Donut, 0.5, 1.0, 0.1, 0.75, "Arial", 0.0, Color.Black, 0.0));

            pieSettingsParameter.PersistentData.Clear();
            pieSettingsParameter.SetPersistentData(defaultFont);
        }
    }
}
