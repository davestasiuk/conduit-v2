using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GH_IO;
using GH_IO.Serialization;
using Grasshopper;
using Grasshopper.Kernel;
using ProvingGround.Conduit.DrawObjects;
using Rhino;
using Rhino.Collections;
using Rhino.Geometry;

namespace ProvingGround.ConduitGH.Components
{
    /// <summary>
    /// Draw mesh component.
    /// </summary>
    /// <seealso cref="Grasshopper.Kernel.GH_Component" />
    public class DrawMeshNode : GH_Component
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DrawMeshNode"/> class.
        /// </summary>
        /// <exclude />
        public DrawMeshNode()
            : base("Draw Mesh", "HUD Mesh", "Draw mesh in the HUD", "Proving Ground", "HUD v2")
        {
        }

        /// <summary>
        /// Returns a consistent ID for this object type. Every object must supply a unique and unchanging
        /// ID that is used to identify objects of the same type.
        /// </summary>
        public override Guid ComponentGuid => new Guid("ed8711b3-41c1-4d6e-8aa9-48fa65d88764");

        /// <summary>
        /// Gets the exposure of this object in the Graphical User Interface.
        /// The default is to expose everywhere.
        /// </summary>
        public override GH_Exposure Exposure => GH_Exposure.tertiary;

        /// <summary>
        /// Override this function to supply a custom icon (24x24 pixels). The result of this property is cached,
        /// so don't worry if icon retrieval is not very fast.
        /// </summary>
        protected override Bitmap Icon => Properties.Resources.PG_Conduit_Mesh;

        /// <summary>
        /// Declare all your input parameters here.
        /// </summary>
        /// <param name="pManager">Use the pManager to register new parameters. pManager is never null.</param>
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            if (pManager == null)
            {
                throw new ArgumentNullException(nameof(pManager));
            }

            pManager.AddMeshParameter("Mesh", "Mesh", "Mesh in the drawing space", GH_ParamAccess.item);
            pManager.AddColourParameter("Color", "Color", "Mesh color", GH_ParamAccess.item, Color.Black);
        }

        /// <summary>
        /// Node outputs.
        /// </summary>
        /// <param name="pManager">Use the pManager to register new parameters. pManager is never null.</param>
        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            if (pManager == null)
            {
                throw new ArgumentNullException(nameof(pManager));
            }

            pManager.AddParameter(new ParamDrawObject { Name = "HUD Drawing Object", NickName = "DrawObj", Access = GH_ParamAccess.item });
        }

        /// <summary>
        /// Code by the component.
        /// </summary>
        /// <param name="dA">Data access.</param>
        protected override void SolveInstance(IGH_DataAccess dA)
        {
            if (dA == null)
            {
                throw new ArgumentNullException(nameof(dA));
            }

            var mesh = new Mesh();
            dA.GetData(0, ref mesh);

            Color color = Color.Black;
            dA.GetData(1, ref color);

            if (mesh == null || !mesh.IsValid)
            {
                AddRuntimeMessage(GH_RuntimeMessageLevel.Error, "Invalid or null mesh");
                return;
            }

            var drawMesh = new DrawMesh(mesh, color);
            var drawObject = new GHDrawObject(drawMesh);

            dA.SetData(0, drawObject);
        }
    }
}
