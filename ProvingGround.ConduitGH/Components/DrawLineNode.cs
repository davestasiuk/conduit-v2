using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GH_IO;
using GH_IO.Serialization;
using Grasshopper;
using Grasshopper.Kernel;
using ProvingGround.Conduit.DrawObjects;
using Rhino;
using Rhino.Collections;
using Rhino.Geometry;

namespace ProvingGround.ConduitGH.Components
{
    /// <summary>
    /// Draw line component.
    /// </summary>
    /// <seealso cref="Grasshopper.Kernel.GH_Component" />
    public class DrawLineNode : GH_Component
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DrawLineNode"/> class.
        /// </summary>
        /// <exclude />
        public DrawLineNode()
            : base("Draw Line", "HUD Line", "Draw line in the HUD", "Proving Ground", "HUD v2")
        {
        }

        /// <summary>
        /// Returns a consistent ID for this object type. Every object must supply a unique and unchanging
        /// ID that is used to identify objects of the same type.
        /// </summary>
        public override Guid ComponentGuid => new Guid("f004c87d-525f-4cae-acdc-a2c533e9cd1c");

        /// <summary>
        /// Gets the exposure of this object in the Graphical User Interface.
        /// The default is to expose everywhere.
        /// </summary>
        public override GH_Exposure Exposure => GH_Exposure.tertiary;

        /// <summary>
        /// Override this function to supply a custom icon (24x24 pixels). The result of this property is cached,
        /// so don't worry if icon retrieval is not very fast.
        /// </summary>
        protected override Bitmap Icon => Properties.Resources.PG_Conduit_Line;

        /// <summary>
        /// Declare all your input parameters here.
        /// </summary>
        /// <param name="pManager">Use the pManager to register new parameters. pManager is never null.</param>
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            if (pManager == null)
            {
                throw new ArgumentNullException(nameof(pManager));
            }

            pManager.AddLineParameter("Line", "Line", "Line in the drawing space", GH_ParamAccess.item);
            pManager.AddIntegerParameter("Thickness", "Thick", "Line thickness in pixels", GH_ParamAccess.item, 1);
            pManager.AddColourParameter("Color", "Color", "Line color", GH_ParamAccess.item, Color.Black);
        }

        /// <summary>
        /// Node outputs.
        /// </summary>
        /// <param name="pManager">Use the pManager to register new parameters. pManager is never null.</param>
        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            if (pManager == null)
            {
                throw new ArgumentNullException(nameof(pManager));
            }

            pManager.AddParameter(new ParamDrawObject { Name = "HUD Drawing Object", NickName = "DrawObj", Access = GH_ParamAccess.item });
        }

        /// <summary>
        /// Code by the component.
        /// </summary>
        /// <param name="dA">Data access.</param>
        protected override void SolveInstance(IGH_DataAccess dA)
        {
            if (dA == null)
            {
                throw new ArgumentNullException(nameof(dA));
            }

            Line line = Line.Unset;
            dA.GetData(0, ref line);

            int thickness = 0;
            dA.GetData(1, ref thickness);

            Color color = Color.Black;
            dA.GetData(2, ref color);

            var drawLine = new DrawLine(line, thickness, color);
            var drawObject = new GHDrawObject(drawLine);

            dA.SetData(0, drawObject);
        }
    }
}
