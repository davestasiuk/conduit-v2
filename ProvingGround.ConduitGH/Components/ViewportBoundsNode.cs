using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;
using GH_IO;
using GH_IO.Serialization;
using Grasshopper;
using Grasshopper.GUI;
using Grasshopper.GUI.Base;
using Grasshopper.GUI.Canvas;
using Grasshopper.Kernel;
using Rhino;
using Rhino.Collections;
using Rhino.Geometry;

namespace ProvingGround.ConduitGH.Components
{
    public class ViewportBoundsNode : GH_Component
    {
        private Rectangle _bounds;

        /// <summary>
        /// Initializes a new instance of the <see cref="ViewportBoundsNode"/> class.
        /// </summary>
        /// <exclude />
        public ViewportBoundsNode()
            : base("Get viewport boundary dimensions", "Bounds", "A rectangle that reflects the current viewport boundary. Refresh to update", "Proving Ground", "HUD v2")
        {
            Rhino.Display.RhinoView activeView = RhinoDoc.ActiveDoc.Views.ActiveView;
            _bounds = activeView.MainViewport.Bounds;
        }

        /// <summary>
        /// Returns a consistent ID for this object type. Every object must supply a unique and unchanging
        /// ID that is used to identify objects of the same type.
        /// </summary>
        public override Guid ComponentGuid => new Guid("c070f04b-15ad-4aa9-953a-f6d511381038");

        /// <summary>
        /// Gets the exposure of this object in the Graphical User Interface.
        /// The default is to expose everywhere.
        /// </summary>
        public override GH_Exposure Exposure => GH_Exposure.primary;

        /// <summary>
        /// Override this function to supply a custom icon (24x24 pixels). The result of this property is cached,
        /// so don't worry if icon retrieval is not very fast.
        /// </summary>
        protected override Bitmap Icon => Properties.Resources.PG_Conduit_Bounds;

        /// <summary>
        /// Read all required data for deserialization from an IO archive.
        /// </summary>
        /// <param name="reader">Object to read with.</param>
        /// <returns>
        /// True on success, false on failure.
        /// </returns>
        public override bool Read(GH_IReader reader)
        {
            if (reader == null)
            {
                throw new ArgumentNullException(nameof(reader));
            }

            int xDimension = 0;
            int yDimension = 0;
            reader.TryGetInt32("XDimension", ref xDimension);
            reader.TryGetInt32("YDimension", ref yDimension);
            _bounds = new Rectangle(0, 0, xDimension, yDimension);
            return base.Read(reader);
        }

        /// <summary>
        /// Write all required data for deserialization to an IO archive.
        /// </summary>
        /// <param name="writer">Object to write with.</param>
        /// <returns>
        /// True on success, false on failure.
        /// </returns>
        public override bool Write(GH_IWriter writer)
        {
            if (writer == null)
            {
                throw new ArgumentNullException(nameof(writer));
            }

            writer.SetInt32("XDimension", _bounds.Width);
            writer.SetInt32("YDimension", _bounds.Height);
            return base.Write(writer);
        }

        /// <summary>
        /// Declare all your input parameters here.
        /// </summary>
        /// <param name="pManager">Use the pManager to register new parameters. pManager is never null.</param>
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            if (pManager == null)
            {
                throw new ArgumentNullException(nameof(pManager));
            }

            pManager.AddBooleanParameter("Refresh", "Refresh", "Refresh the Boundary Dimension", GH_ParamAccess.item, true);
            pManager.AddPointParameter("Origin", "Origin", "Origin point for showing rectangle", GH_ParamAccess.item, new Point3d(0, 0, 0));
            pManager.AddNumberParameter("Scale", "Scale", "Scale of rectangle", GH_ParamAccess.item, 0.25);
        }

        /// <summary>
        /// Declare all your output parameters here.
        /// </summary>
        /// <param name="pManager">Use the pManager to register new parameters. pManager is never null.</param>
        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            if (pManager == null)
            {
                throw new ArgumentNullException(nameof(pManager));
            }

            pManager.Register_RectangleParam("Bounds", "Bounds", "The boundary dimensions of the active viewport");
        }

        /// <summary>
        /// This function will be called (successively) from within the
        /// ComputeData method of this component.
        /// </summary>
        /// <param name="dA">Data Access object. Use this object to retrieve data
        /// from input parameters and assign data to output parameters.</param>
        protected override void SolveInstance(IGH_DataAccess dA)
        {
            if (dA == null)
            {
                throw new ArgumentNullException(nameof(dA));
            }

            bool refresh = false;
            dA.GetData(0, ref refresh);

            Point3d origin = Point3d.Unset;
            dA.GetData(1, ref origin);

            double scale = double.NaN;
            dA.GetData(2, ref scale);

            if (refresh)
            {
                Rhino.Display.RhinoView m_activeView = RhinoDoc.ActiveDoc.Views.ActiveView;
                _bounds = m_activeView.MainViewport.Bounds;
            }

            Plane setPlane = Plane.WorldXY;
            setPlane.Origin = origin;

            var outputBounds = new Rectangle3d(
                setPlane,
                _bounds.Width * scale,
                _bounds.Height * scale);

            dA.SetData(0, outputBounds);
        }
    }
}
