using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Grasshopper.Kernel;
using ProvingGround.Conduit.ObjectSettings;
using ProvingGround.ConduitGH.Settings;

namespace ProvingGround.ConduitGH.Components
{
    public class PieSettingsNode : GH_Component
    {
        public PieSettingsNode()
            : base("HUD Pie Settings", "Pie Settings", "Settings for pie charts in the Conduit HUD", "Proving Ground", "HUD v2")
        {
        }

        public override Guid ComponentGuid => new Guid("8875de7f-699c-46b2-925f-20cd65749ba4");

        public override GH_Exposure Exposure => GH_Exposure.secondary;

        protected override Bitmap Icon => Properties.Resources.PG_Conduit_PieStyle;

        protected override void RegisterInputParams(GH_InputParamManager pManager)
        {
            if (pManager == null)
            {
                throw new ArgumentNullException(nameof(pManager));
            }

            pManager.AddNumberParameter("Outer radius", "Outer", "Outer radius from center relative to boundary (1.0 = maximum)", GH_ParamAccess.item, 1.0);
            pManager.AddNumberParameter("Inner radius", "Inner", "Outer radius from center relative to boundary (0.0 = pie chart, should be less than outer radius)", GH_ParamAccess.item, 0.5);
            pManager.AddNumberParameter("Label height", "Lbl Ht", "Label height as percentage of boundary dimension", GH_ParamAccess.item, 0.075);
            pManager.AddNumberParameter("Label radius", "Lbl Rad", "Label location from center relative to bounary (1.0 = maximum)", GH_ParamAccess.item, 0.75);
            pManager.AddTextParameter("Label font", "Lbl Font", "Font name for labels", GH_ParamAccess.item, "Arial");
            pManager.AddNumberParameter("Label offset", "Lbl Offset", "Additional label alignment offset from center (1.0 = maximum)", GH_ParamAccess.item, 0.0);
            pManager.AddColourParameter("Label color", "Lbl Clr", "The text color for labels", GH_ParamAccess.item, Color.Black);
            pManager.AddNumberParameter("Start angle", "Angle", "The starting angle (in radians) for the chart (0 = 3 o'clock)", GH_ParamAccess.item, 0.0);
        }

        protected override void RegisterOutputParams(GH_OutputParamManager pManager)
        {
            if (pManager == null)
            {
                throw new ArgumentNullException(nameof(pManager));
            }

            pManager.AddParameter(new ParamPieSettings { Name = "Pie Settings", NickName = "Pie", Description = "Settings for pie charts in the Conduit HUD", Access = GH_ParamAccess.item });
        }

        protected override void SolveInstance(IGH_DataAccess dA)
        {
            if (dA == null)
            {
                throw new ArgumentNullException(nameof(dA));
            }

            double outerRadius = 0;
            dA.GetData(0, ref outerRadius);

            double innerRadius = 0;
            dA.GetData(1, ref innerRadius);

            PieType pieType = innerRadius == 0.0 ? PieType.Pie : PieType.Donut;

            double labelHeight = 0;
            dA.GetData(2, ref labelHeight);

            double labelRadius = 0;
            dA.GetData(3, ref labelRadius);

            string labelFont = null;
            dA.GetData(4, ref labelFont);

            double labelOffset = 0;
            dA.GetData(5, ref labelOffset);

            Color labelColor = default;
            dA.GetData(6, ref labelColor);

            double startAngle = 0;
            dA.GetData(7, ref startAngle);

            var pieSettings = new PieSettings(pieType, innerRadius, outerRadius, labelHeight, labelRadius, labelFont, labelOffset, labelColor, startAngle);
            dA.SetData(0, new GHPieSettings(pieSettings));
        }
    }
}
