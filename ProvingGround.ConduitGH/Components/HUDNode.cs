using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GH_IO.Serialization;
using Grasshopper;
using Grasshopper.GUI;
using Grasshopper.Kernel;
using ProvingGround.Conduit;
using ProvingGround.Conduit.DrawObjects;
using Rhino;
using Rhino.Geometry;

namespace ProvingGround.ConduitGH.Components
{
    public class HUDNode : GH_Component
    {
        /// <summary>
        /// The main heads up display (HUD).
        /// </summary>
        private HUD _hud;

        /// <summary>
        /// The HUD is visible when show is set to true.
        /// </summary>
        private bool _show = false;

        /// <summary>
        /// A comma-separated string to filter the viewport(s) that this HUD is drawn in.
        /// </summary>
        [System.Runtime.CompilerServices.CompilerGenerated]
        private string _viewportFilter;

        /// <summary>
        /// Initializes a new instance of the <see cref="HUDNode"/> class.
        /// </summary>
        /// <exclude />
        public HUDNode()
            : base("Heads Up Display (HUD)", "HUD", "The core node for drawing a heads up display.", "Proving Ground", "HUD v2")
        {
            _viewportFilter = string.Empty;
        }

        /// <summary>
        /// Returns a consistent ID for this object type. Every object must supply a unique and unchanging
        /// ID that is used to identify objects of the same type.
        /// </summary>
        public override Guid ComponentGuid => new Guid("dd4176f5-cf25-44c6-8e6b-4ef420bd4efa");

        /// <summary>
        /// Gets the exposure of this object in the Graphical User Interface.
        /// The default is to expose everywhere.
        /// </summary>
        public override GH_Exposure Exposure => GH_Exposure.quinary;

        /// <summary>
        /// Override this function to supply a custom icon (24x24 pixels). The result of this property is cached,
        /// so don't worry if icon retrieval is not very fast.
        /// </summary>
        protected override Bitmap Icon => Properties.Resources.PG_Conduit_HUD;

        /// <summary>
        /// Overrides the DocumentContextChanged method and delegates the call to all parameters.
        /// </summary>
        /// <param name="document">Document that owns this object.</param>
        /// <param name="context">The reason for this event.</param>
        public override void DocumentContextChanged(GH_Document document, GH_DocumentContext context)
        {
            if (_hud != null && _show && ( context == GH_DocumentContext.Close || context == GH_DocumentContext.Unloaded ))
            {
                _hud.Enabled = false;
            }

            base.DocumentContextChanged(document, context);
        }

        /// <summary>
        /// Read all required data for deserialization from an IO archive.
        /// </summary>
        /// <param name="reader">Object to read with.</param>
        /// <returns>
        /// True on success, false on failure.
        /// </returns>
        public override bool Read(GH_IReader reader)
        {
            if (reader == null)
            {
                throw new ArgumentNullException(nameof(reader));
            }

            string retrievedFilter = string.Empty;
            if (reader.TryGetString("ViewportFilter", ref retrievedFilter))
            {
                _viewportFilter = retrievedFilter;
                Message = _viewportFilter;
            }

            return base.Read(reader);
        }

        /// <summary>
        /// Overrides the RemovedFromDocument method and delegates the call to all parameters.
        /// </summary>
        /// <param name="document">Document that now no longer owns this object.</param>
        public override void RemovedFromDocument(GH_Document document)
        {
            if (_hud != null && _show)
            {
                _hud.Enabled = false;
            }

            base.RemovedFromDocument(document);
        }

        /// <summary>
        /// Write all required data for deserialization to an IO archive.
        /// </summary>
        /// <param name="writer">Object to write with.</param>
        /// <returns>
        /// True on success, false on failure.
        /// </returns>
        public override bool Write(GH_IWriter writer)
        {
            if (writer == null)
            {
                throw new ArgumentNullException(nameof(writer));
            }

            writer.SetString("ViewportFilter", _viewportFilter);
            return base.Write(writer);
        }

        /// <summary>
        /// Override this function if you want to insert some custom menu items in your derived Component class.
        /// Items will be added between List Matching items and parameter menus.
        /// This instance adds the ability for the user to filter the viewport(s) in which the HUD should be shown.
        /// </summary>
        /// <param name="menu">Menu to append to.</param>
        protected override void AppendAdditionalComponentMenuItems(ToolStripDropDown menu)
        {
            ToolStripMenuItem item = Menu_AppendItem(menu, "Viewport Filter");
            ToolStripTextBox vpItem = Menu_AppendTextItem(menu, _viewportFilter, new GH_MenuTextBox.KeyDownEventHandler(ViewportFilterKeyDown), null, false);
            Menu_AppendItem(menu, "Save SVG", SaveSVG);
            base.AppendAdditionalComponentMenuItems(menu);
        }

        /// <summary>
        /// Declare all your input parameters here.
        /// </summary>
        /// <param name="pManager">Use the pManager to register new parameters. pManager is never null.</param>
        protected override void RegisterInputParams(GH_InputParamManager pManager)
        {
            if (pManager == null)
            {
                throw new ArgumentNullException(nameof(pManager));
            }

            pManager.AddRectangleParameter("Drawing Boundary", "Bounds", "Rectangular sketch container for objects (in the XY Plane)", GH_ParamAccess.item);
            pManager.AddParameter(new ParamDrawObject { Name = "Drawing Objects", NickName = "DrawObjs", Description = "Objects to draw in HUD", Access = GH_ParamAccess.list });
            pManager.AddBooleanParameter("Show HUD", "Show", "Toggle for showing HUD", GH_ParamAccess.item, false);
        }

        /// <summary>
        /// Declare all your output parameters here.
        /// </summary>
        /// <param name="pManager">Use the pManager to register new parameters. pManager is never null.</param>
        protected override void RegisterOutputParams(GH_OutputParamManager pManager)
        {
        }

        /// <summary>
        /// This function will be called (successively) from within the
        /// ComputeData method of this component.
        /// </summary>
        /// <param name="dA">Data Access object. Use this object to retrieve data
        /// from input parameters and assign data to output parameters.</param>
        protected override void SolveInstance(IGH_DataAccess dA)
        {
            if (dA == null)
            {
                throw new ArgumentNullException(nameof(dA));
            }

            Rectangle3d bounds = Rectangle3d.Unset;
            dA.GetData(0, ref bounds);

            var ghDrawObjects = new List<GHDrawObject>();
            dA.GetDataList(1, ghDrawObjects);

            dA.GetData(2, ref _show);

            if (_hud != null)
            {
                _hud.Enabled = false;
            }

            if (_show)
            {
                var drawObjects = new List<DrawObject>();
                for (int i = 0; i < ghDrawObjects.Count; i++)
                {
                    drawObjects.Add(ghDrawObjects[i].Value);
                }

                string[] viewportFilters = _viewportFilter.Split(new char[] { ',', ' ' }, StringSplitOptions.RemoveEmptyEntries);

                _hud = new HUD(bounds, drawObjects, viewportFilters);
                _hud.PrepareDrawObjects();
                _hud.Enabled = true;
            }
        }

        private void SaveSVG(object sender, EventArgs e)
        {
            if (_hud != null)
            {
                string filePath = string.Empty;

                using (var saveFileDialog1 = new SaveFileDialog
                {
                    Filter = "(SVG)|*.svg",
                })
                {
                    if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                    {
                        filePath = saveFileDialog1.FileName;
                    }
                }

                if (!string.IsNullOrEmpty(filePath))
                {
                    _hud.ViewAndSVGCapture(filePath, RhinoDoc.ActiveDoc, 1.0);
                }
            }
        }

        /// <summary>
        /// Handles updating and refreshing user-modified viewport filter values.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="KeyEventArgs"/> instance containing the event data.</param>
        private void ViewportFilterKeyDown(GH_MenuTextBox sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return)
            {
                if (string.Compare(_viewportFilter, sender.Text, false, System.Globalization.CultureInfo.CurrentCulture) == 0)
                {
                    return;
                }

                RecordUndoEvent("Filter: " + sender.Text);
                _viewportFilter = sender.Text;
                Message = sender.Text;
                Attributes.ExpireLayout();
                ExpireSolution(true);
                Instances.RedrawAll();
            }
        }
    }
}
