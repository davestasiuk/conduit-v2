using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Grasshopper.Kernel;
using ProvingGround.Conduit.ObjectSettings;
using ProvingGround.ConduitGH.Settings;

namespace ProvingGround.ConduitGH.Components
{
    public class TextSettingsNode : GH_Component
    {
        public TextSettingsNode()
            : base("HUD Font", "HUD Font", "Font settings for text objects in the Conduit HUD", "Proving Ground", "HUD v2")
        {
        }

        public override Guid ComponentGuid => new Guid("0c438fab-5b98-4404-ba9c-6d4ccc1761b6");

        public override GH_Exposure Exposure => GH_Exposure.secondary;

        protected override Bitmap Icon => Properties.Resources.PG_Conduit_Font;

        protected override void RegisterInputParams(GH_InputParamManager pManager)
        {
            if (pManager == null)
            {
                throw new ArgumentNullException(nameof(pManager));
            }

            pManager.AddTextParameter("Font name", "Font", "The name of the font", GH_ParamAccess.item, "Arial");
            pManager.AddNumberParameter("Height", "Height", "Height of the text (in pixels)", GH_ParamAccess.item, 50);
            pManager.AddNumberParameter("Padding", "Pad", "Additional padding between multiple lines of text (in pixels)", GH_ParamAccess.item, 5);
            pManager.AddBooleanParameter("Bold", "Bold", "True if the text should be bold-faced", GH_ParamAccess.item, false);
            pManager.AddBooleanParameter("Italic", "Italic", "True if the text should be italicized", GH_ParamAccess.item, false);
            pManager.AddColourParameter("Color", "Color", "The text color", GH_ParamAccess.item, Color.Black);
        }

        protected override void RegisterOutputParams(GH_OutputParamManager pManager)
        {
            if (pManager == null)
            {
                throw new ArgumentNullException(nameof(pManager));
            }

            pManager.AddParameter(new ParamTextSettings { Name = "Text Font", NickName = "Font", Description = "Custom font for text objects in the Conduit HUD", Access = GH_ParamAccess.item });
        }

        protected override void SolveInstance(IGH_DataAccess dA)
        {
            if (dA == null)
            {
                throw new ArgumentNullException(nameof(dA));
            }

            string fontName = null;
            dA.GetData(0, ref fontName);

            double pixelHeight = 0;
            dA.GetData(1, ref pixelHeight);

            double pixelPadding = 0;
            dA.GetData(2, ref pixelPadding);

            bool bold = false;
            dA.GetData(3, ref bold);

            bool italic = false;
            dA.GetData(4, ref italic);

            Color color = default;
            dA.GetData(5, ref color);

            var font = new TextSettings(fontName, pixelHeight, pixelPadding, bold, italic, color);
            dA.SetData(0, new GHTextSettings(font));
        }
    }
}
