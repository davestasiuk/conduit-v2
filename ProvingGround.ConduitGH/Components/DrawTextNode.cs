using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Grasshopper.Kernel;
using ProvingGround.Conduit.DrawObjects;
using ProvingGround.Conduit.ObjectSettings;
using ProvingGround.ConduitGH.Settings;
using Rhino.Geometry;

namespace ProvingGround.ConduitGH.Components
{
    public class DrawTextNode : GH_Component
    {
        public DrawTextNode()
            : base("Draw Text", "HUD Texte", "Draw a text list in the HUD", "Proving Ground", "HUD v2")
        {
        }

        public override Guid ComponentGuid => new Guid("909abdf1-b6fc-4ffd-860b-651f208cc848");

        public override GH_Exposure Exposure => GH_Exposure.tertiary;

        protected override Bitmap Icon => Properties.Resources.PG_Conduit_Text;

        public override void DocumentContextChanged(GH_Document document, GH_DocumentContext context)
        {
            if (context == GH_DocumentContext.Open)
            {
                ParamTextSettings fontParameter = Params.Input[4] as ParamTextSettings;
                if (fontParameter.Sources.Count == 0)
                {
                    SetDefaultFont(fontParameter);
                }
            }

            base.DocumentContextChanged(document, context);
        }

        protected override void RegisterInputParams(GH_InputParamManager pManager)
        {
            if (pManager == null)
            {
                throw new ArgumentNullException(nameof(pManager));
            }

            pManager.AddRectangleParameter("Boundary", "Bounds", "Boundary for text", GH_ParamAccess.item);
            pManager.AddTextParameter("Text", "Text", "Text lines to write", GH_ParamAccess.list);
            pManager.AddIntegerParameter("Justification", "Just", "Text justification (0=Left, 1=Center, 2=Right)", GH_ParamAccess.item, 0);
            pManager.AddIntegerParameter("Distribution", "Distrib", "Distibution of text (0=Distribute over full boundary, 1=Absolute by text height)", GH_ParamAccess.item, 0);

            var fontParameter = new ParamTextSettings { Name = "Text Font", NickName = "Font", Description = "Custom fonts", Access = GH_ParamAccess.list };
            SetDefaultFont(fontParameter);
            pManager.AddParameter(fontParameter);
        }

        protected override void RegisterOutputParams(GH_OutputParamManager pManager)
        {
            if (pManager == null)
            {
                throw new ArgumentNullException(nameof(pManager));
            }

            pManager.AddParameter(new ParamDrawObject { Name = "HUD Drawing Object", NickName = "DrawObj", Access = GH_ParamAccess.item });
        }

        protected override void SolveInstance(IGH_DataAccess dA)
        {
            if (dA == null)
            {
                throw new ArgumentNullException(nameof(dA));
            }

            Rectangle3d bounds = Rectangle3d.Unset;
            dA.GetData(0, ref bounds);

            var text = new List<string>();
            dA.GetDataList(1, text);

            int justificationInt = 0;
            dA.GetData(2, ref justificationInt);

            DrawText.Justification justification =
                justificationInt == 0 ? DrawText.Justification.Left :
                justificationInt == 1 ? DrawText.Justification.Center :
                DrawText.Justification.Right;

            int distributionInt = 0;
            dA.GetData(3, ref distributionInt);

            DrawText.Distribution distribution =
                distributionInt == 0 ? DrawText.Distribution.Relative :
                DrawText.Distribution.Absolute;

            var ghFonts = new List<GHTextSettings>();
            dA.GetDataList(4, ghFonts);

            var fonts = new List<TextSettings>();

            for (int i = 0; i < ghFonts.Count; i++)
            {
                fonts.Add(ghFonts[i].Value);
            }

            DrawText drawText = new DrawText(bounds, text, fonts, distribution, justification);
            GHDrawObject drawObject = new GHDrawObject(drawText);

            dA.SetData(0, drawObject);
        }

        private void SetDefaultFont(ParamTextSettings fontParameter)
        {
            GHTextSettings defaultFont = new GHTextSettings(new TextSettings("Arial", 50, 5, false, false, Color.Black));

            fontParameter.PersistentData.Clear();
            fontParameter.SetPersistentData(defaultFont);
        }
    }
}
