using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Grasshopper.Kernel;
using Grasshopper.Kernel.Types;
using ProvingGround.Conduit.DrawObjects;

namespace ProvingGround.ConduitGH
{
    /// <summary>
    /// Custom GH_Goo for HUD drawing objects.
    /// </summary>
    /// <seealso cref="Grasshopper.Kernel.Types.GH_Goo{ProvingGround.Conduit.DrawObjects.DrawObject}" />
    public class GHDrawObject : GH_Goo<DrawObject>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GHDrawObject"/> class.
        /// </summary>
        public GHDrawObject()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GHDrawObject"/> class.
        /// </summary>
        /// <param name="drawObject">The draw object.</param>
        public GHDrawObject(DrawObject drawObject)
        {
            Value = drawObject;
        }

        /// <summary>
        /// Returns true if ... is valid.
        /// </summary>
        public override bool IsValid => Value != null;

        /// <summary>
        /// Gets a description of the type of the implementation.
        /// </summary>
        public override string TypeDescription => "A Conduit HUD Drawing Object";

        /// <summary>
        /// Gets the name of the type of the implementation.
        /// </summary>
        public override string TypeName => "DrawObject";

        /// <summary>
        /// Make a complete duplicate of this instance. No shallow copies.
        /// </summary>
        /// <returns>A deep copy of the draw object.</returns>
        /// <remarks>
        /// Classes which implement this interface should also provide type specific Duplicate methods.
        /// </remarks>
        public override IGH_Goo Duplicate()
        {
            if (Value != null)
            {
                return new GHDrawObject(Value);
            }

            return null;
        }

        /// <summary>
        /// Converts to string.
        /// </summary>
        /// <returns>
        /// A <see cref="string" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            if (Value == null)
            {
                return "null Drawing Object";
            }
            else
            {
                return Value.ObjectType;
            }
        }
    }
}
