using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Grasshopper.Kernel;

namespace ProvingGround.ConduitGH
{
    /// <summary>
    /// A custom GH_Param for HUD drawing objects.
    /// </summary>
    /// <seealso cref="Grasshopper.Kernel.GH_PersistentParam{ProvingGround.Conduit.DrawObjects.GH_DrawObject}" />
    public class ParamDrawObject : GH_PersistentParam<GHDrawObject>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ParamDrawObject"/> class.
        /// </summary>
        public ParamDrawObject()
            : base("HUD Drawing Object", "DrawObject", "A custom Drawing Object for the Conduit HUD", "Proving Ground", "HUD v2")
        {
        }

        /// <summary>
        /// Returns a consistent ID for this object type. Every object must supply a unique and unchanging
        /// ID that is used to identify objects of the same type.
        /// </summary>
        public override Guid ComponentGuid => new Guid("c66be1b4-840d-4353-a7de-032b4e566e6d");

        /// <summary>
        /// Gets the exposure of this object in the Graphical User Interface.
        /// The default is to expose everywhere.
        /// </summary>
        public override GH_Exposure Exposure => GH_Exposure.hidden;

        /// <summary>
        /// Override this function to supply a custom icon (24x24 pixels). The result of this property is cached,
        /// so don't worry if icon retrieval is not very fast.
        /// </summary>
        protected override Bitmap Icon => Properties.Resources.PG_Conduit_DrawingObject;

        /// <summary>
        /// Prompts the plural.
        /// </summary>
        /// <param name="values">The values.</param>
        /// <returns>Nothing.</returns>
        protected override GH_GetterResult Prompt_Plural(ref List<GHDrawObject> values)
        {
            return GH_GetterResult.cancel;
        }

        /// <summary>
        /// Prompts the singular.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>Nothing.</returns>
        protected override GH_GetterResult Prompt_Singular(ref GHDrawObject value)
        {
            return GH_GetterResult.cancel;
        }
    }
}
