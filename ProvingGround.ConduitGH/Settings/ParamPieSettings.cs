using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Grasshopper.Kernel;

namespace ProvingGround.ConduitGH.Settings
{
    public class ParamPieSettings : GH_PersistentParam<GHPieSettings>
    {
        public ParamPieSettings()
            : base("HUD Pie Settings", "Pie Settings", "Settings for a Pie chart in the Conduit HUD", "Proving Ground", "HUD v2")
        {
        }

        /// <summary>
        /// Returns a consistent ID for this object type. Every object must supply a unique and unchanging
        /// ID that is used to identify objects of the same type.
        /// </summary>
        public override Guid ComponentGuid => new Guid("75148a7e-7ba1-4ad1-9516-8af67d26e728");

        /// <summary>
        /// Gets the exposure of this object in the Graphical User Interface.
        /// The default is to expose everywhere.
        /// </summary>
        public override GH_Exposure Exposure => GH_Exposure.hidden;

        /// <summary>
        /// Override this function to supply a custom icon (24x24 pixels). The result of this property is cached,
        /// so don't worry if icon retrieval is not very fast.
        /// </summary>
        protected override Bitmap Icon => Properties.Resources.PG_Conduit_Font;

        /// <summary>
        /// Prompts the plural.
        /// </summary>
        /// <param name="values">The values.</param>
        /// <returns>Nothing.</returns>
        protected override GH_GetterResult Prompt_Plural(ref List<GHPieSettings> values)
        {
            return GH_GetterResult.cancel;
        }

        /// <summary>
        /// Prompts the singular.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>Nothing.</returns>
        protected override GH_GetterResult Prompt_Singular(ref GHPieSettings value)
        {
            return GH_GetterResult.cancel;
        }
    }
}
