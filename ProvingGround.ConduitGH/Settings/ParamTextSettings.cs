using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Grasshopper.Kernel;

namespace ProvingGround.ConduitGH.Settings
{
    public class ParamTextSettings : GH_PersistentParam<GHTextSettings>
    {
        public ParamTextSettings()
            : base("HUD Font", "Font", "Font setting for the Conduit HUD", "Proving Ground", "HUD v2")
        {
        }

        /// <summary>
        /// Returns a consistent ID for this object type. Every object must supply a unique and unchanging
        /// ID that is used to identify objects of the same type.
        /// </summary>
        public override Guid ComponentGuid => new Guid("0a7a50d3-580e-48b2-8c0c-39a97d8ea53c");

        /// <summary>
        /// Gets the exposure of this object in the Graphical User Interface.
        /// The default is to expose everywhere.
        /// </summary>
        public override GH_Exposure Exposure => GH_Exposure.hidden;

        /// <summary>
        /// Override this function to supply a custom icon (24x24 pixels). The result of this property is cached,
        /// so don't worry if icon retrieval is not very fast.
        /// </summary>
        protected override Bitmap Icon => Properties.Resources.PG_Conduit_Font;

        /// <summary>
        /// Prompts the plural.
        /// </summary>
        /// <param name="values">The values.</param>
        /// <returns>Nothing.</returns>
        protected override GH_GetterResult Prompt_Plural(ref List<GHTextSettings> values)
        {
            return GH_GetterResult.cancel;
        }

        /// <summary>
        /// Prompts the singular.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>Nothing.</returns>
        protected override GH_GetterResult Prompt_Singular(ref GHTextSettings value)
        {
            return GH_GetterResult.cancel;
        }
    }
}
