using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Grasshopper.Kernel.Types;
using ProvingGround.Conduit.ObjectSettings;

namespace ProvingGround.ConduitGH.Settings
{
    public class GHPieSettings : GH_Goo<PieSettings>
    {
        public GHPieSettings()
        {
        }

        public GHPieSettings(PieSettings pieSettings)
        {
            Value = pieSettings;
        }

        /// <summary>
        /// Returns true if ... is valid.
        /// </summary>
        public override bool IsValid => Value != null;

        /// <summary>
        /// Gets a description of the type of the implementation.
        /// </summary>
        public override string TypeDescription => "Settings for a Conduit HUD Pie chart";

        /// <summary>
        /// Gets the name of the type of the implementation.
        /// </summary>
        public override string TypeName => "PieSettings";

        /// <summary>
        /// Make a complete duplicate of this instance. No shallow copies.
        /// </summary>
        /// <returns>A copy of these pie settings.</returns>
        /// <remarks>
        /// Classes which implement this interface should also provide type specific Duplicate methods.
        /// </remarks>
        public override IGH_Goo Duplicate()
        {
            if (Value != null)
            {
                return new GHPieSettings(Value);
            }

            return null;
        }

        /// <summary>
        /// Converts to string.
        /// </summary>
        /// <returns>
        /// A <see cref="string" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            if (Value == null)
            {
                return "null Pie settings";
            }
            else
            {
                return "HUD Pie settings";
            }
        }
    }
}
