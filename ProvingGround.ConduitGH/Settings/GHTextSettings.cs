using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Grasshopper.Kernel;
using Grasshopper.Kernel.Types;
using ProvingGround.Conduit.DrawObjects;
using ProvingGround.Conduit.ObjectSettings;

namespace ProvingGround.ConduitGH.Settings
{
    public class GHTextSettings : GH_Goo<TextSettings>
    {
        public GHTextSettings()
        {
        }

        public GHTextSettings(TextSettings textSettings)
        {
            Value = textSettings;
        }

        /// <summary>
        /// Returns true if ... is valid.
        /// </summary>
        public override bool IsValid => Value != null;

        /// <summary>
        /// Gets a description of the type of the implementation.
        /// </summary>
        public override string TypeDescription => "A Conduit HUD Text Font";

        /// <summary>
        /// Gets the name of the type of the implementation.
        /// </summary>
        public override string TypeName => "TextSettings";

        /// <summary>
        /// Make a complete duplicate of this instance. No shallow copies.
        /// </summary>
        /// <returns>A copy of this font.</returns>
        /// <remarks>
        /// Classes which implement this interface should also provide type specific Duplicate methods.
        /// </remarks>
        public override IGH_Goo Duplicate()
        {
            if (Value != null)
            {
                return new GHTextSettings(Value);
            }

            return null;
        }

        /// <summary>
        /// Converts to string.
        /// </summary>
        /// <returns>
        /// A <see cref="string" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            if (Value == null)
            {
                return "null Font";
            }
            else
            {
                return "HUD Font";
            }
        }
    }
}
